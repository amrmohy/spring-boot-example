package com.code.res.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity(name = "Category")
@Table(name = "category")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Category {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;
    @Column(name = "category_name",columnDefinition = "TEXT")
    private String categoryName;

    @OneToMany(mappedBy = "category")
    Set<Order> orders = new HashSet<>();
    @CreationTimestamp
    @Column(name = "created_at")
    private LocalDateTime createdAt;
}
